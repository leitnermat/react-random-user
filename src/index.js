import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import MyComponent from './components/default';
import './styles/global.scss';

const ReactRoot = document.getElementById('react-root');

const load = async () => {
  const response = await axios.get('https://randomuser.me/api/');
  const {data} = response;
  console.log(data);
  ReactDOM.render(
      <MyComponent
        picture={data.results[0].picture.large}
        name={data.results[0].name.first}
        lastname={data.results[0].name.last}
        username={data.results[0].login.username}
        email={data.results[0].email}
        phone={data.results[0].phone}
        age={data.results[0].dob.age}
      />,
      ReactRoot);
};
load();
