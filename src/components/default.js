import React from 'react';
import './style.scss';

class MyComponent extends React.Component {
  handleClick(value) {
    console.log(value);
    value.style.textDecoration = 'line-through';
  }
  render() {
    return (
      <div className="card">
        <img src={this.props.picture} />
        <h1 className="name">{this.props.name} {this.props.lastname}</h1>
        <div className="info__container">
          <div onClick={() => this.handleClick(this.usernameValue)}>
            <label>
              Nom d'utilisateur: </label>
            <span
              className="value"
              ref={usernameValue => (this.usernameValue = usernameValue)}>
              {this.props.username}</span>
          </div>
          <div onClick={() => this.handleClick(this.ageValue)}>
            <label>Age: </label>
            <span
              className="value"
              ref={ageValue => (this.ageValue = ageValue)}>
              {this.props.age}</span>
          </div>
          <div onClick={() => this.handleClick(this.emailValue)}>
            <label>Email: </label>
            <span
              className="value"
              ref={emailValue => (this.emailValue = emailValue)}>
              {this.props.email}</span>
          </div>
          <div onClick={() => this.handleClick(this.telValue)}>
            <label>Tél: </label>
            <span
              className="value"
              ref={telValue => (this.telValue = telValue)}>
              {this.props.phone}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default MyComponent;
